package com.yl.knowledgelibray.common;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 11:11
 * To change this template use File | Settings | File Templates.
 */
public class FileUtil {

    public static void uploadFile(byte[] file, String filePath, String fileName) throws Exception {
        File targetFile = new File(filePath);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }
        FileOutputStream out = new FileOutputStream(filePath+fileName);
        out.write(file);
        out.flush();
        out.close();
    }

}
