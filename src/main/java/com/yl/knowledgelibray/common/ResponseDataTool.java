package com.yl.knowledgelibray.common;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.Knowledge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/23
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
public class ResponseDataTool {
    private ResponseDataTool() {}

    private static ResponseDataTool single = new ResponseDataTool();

    // 静态工厂方法
    public static ResponseDataTool getTool() {
        return single;
    }

    public HashMap returnListContent(List data){
        HashMap ret = new HashMap();
        ret.put("data",data);
        return ret;
    }

    public HashMap returnPageableData(Pageable pageable, Page pages) {
        HashMap ret = new HashMap();
        ret.put("first",pages.isFirst());
        ret.put("last",pages.isLast());
        ret.put("number",pages.getNumber());
        ret.put("totalPages",pages.getTotalPages());
        ret.put("numberOfElements",pages.getNumberOfElements());
        ret.put("totalElements",pages.getTotalElements());

        List<Knowledge> sublist = null;
        System.out.println(JSON.toJSONString(pages.getContent()));
        if (pageable.getOffset() > pages.getContent().size()) {
            sublist = new LinkedList<>();
        }else if (pageable.getOffset() <= pages.getContent().size() && pageable.getOffset() + pageable.getPageSize() > pages.getContent().size()) {
            sublist = pages.getContent().subList((int) pageable.getOffset(), pages.getContent().size());
        }else if(pageable.getPageNumber() == 0 && pageable.getPageSize() == 0){
            //默认返回所有
            sublist = pages.getContent();
        }else{
            sublist = pages.getContent().subList((int)pageable.getOffset(), (int)pageable.getOffset() + pageable.getPageSize());
        }

        ret.put("data",sublist);
        return ret;
    }

    public HashMap returnHashMapContent(HashMap data){
        HashMap ret = new HashMap();
        ret.put("data",data);
        return ret;
    }

    public HashMap returnContent(Object object) {
        HashMap ret = new HashMap();
        ret.put("data",object);
        return ret;
    }

    /**
     * 时间戳转化
     * */
    public static String timeStamp2Date(String seconds,String format) {
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return "";
        }
        if(format == null || format.isEmpty()){
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds+"000")));
     }

}
