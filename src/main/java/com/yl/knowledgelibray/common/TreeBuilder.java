package com.yl.knowledgelibray.common;

import com.yl.knowledgelibray.domain.KnowledgeType;
import net.sf.json.JSONArray;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */

public class TreeBuilder {

    List<KnowledgeType> nodes = new ArrayList<>();

    public List<KnowledgeType> buildTree(List<KnowledgeType> nodes) {

        TreeBuilder treeBuilder = new TreeBuilder(nodes);

        return treeBuilder.buildJSONTree();
    }

    public TreeBuilder() {
    }

    public TreeBuilder(List<KnowledgeType> nodes) {
        super();
        this.nodes = nodes;
    }

    // 构建JSON树形结构
    public List<KnowledgeType> buildJSONTree() {
        List<KnowledgeType> nodeTree = buildTree();
        return nodeTree;
    }

    // 构建树形结构
    public List<KnowledgeType> buildTree() {
        List<KnowledgeType> treeNodes = new ArrayList<>();
        List<KnowledgeType> rootNodes = getRootNodes();
        for (KnowledgeType rootNode : rootNodes) {
            buildChildNodes(rootNode);
            treeNodes.add(rootNode);
        }
        return treeNodes;
    }

    //合并子节点
    public void buildChildNodes(KnowledgeType node) {
        List<KnowledgeType> children = getChildNodes(node);
        if (!children.isEmpty()) {
            node.setChildren(children);
        }
    }

    // 获取父节点下所有的子节点
    public List<KnowledgeType> getChildNodes(KnowledgeType pnode) {
        List<KnowledgeType> childNodes = new ArrayList<>();
        for (KnowledgeType n : nodes) {
            if (pnode.getTypeCode().equals(n.getFatherCode()) && n.getTypeCode().equals(n.getFatherCode()) == false) {
                childNodes.add(n);
            }
        }
        return childNodes;
    }

    // 判断是否为根节点
    public boolean rootNode(KnowledgeType node) {
        boolean isRootNode = false;
        for (KnowledgeType n : nodes) {
            if (node.getFatherCode() == null || node.getFatherCode().equals("")) {
                isRootNode = true;
                break;
            }
        }
        return isRootNode;
    }

    // 获取集合中所有的根节点
    public List<KnowledgeType> getRootNodes() {
        List<KnowledgeType> rootNodes = new ArrayList<>();
        for (KnowledgeType n : nodes) {
            if (rootNode(n)) {
                rootNodes.add(n);
            }
        }
        return rootNodes;
    }
}
