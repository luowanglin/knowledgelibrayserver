package com.yl.knowledgelibray.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 12:39
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class ApplicationConfig implements WebMvcConfigurer {

    @Autowired
    UserConfig userConfig;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/**")
//                .addResourceLocations("classpath:/META-INF/resources/")
//                .addResourceLocations("classpath:/resources/")
//                .addResourceLocations("classpath:/static/")
//                .addResourceLocations("classpath:/public/");
        registry.addResourceHandler("/file/**").addResourceLocations("file:"+System.getProperty("user.dir")+"/uploads/knowledge/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(userConfig);
    }
}
