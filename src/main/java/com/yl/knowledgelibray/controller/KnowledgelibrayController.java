package com.yl.knowledgelibray.controller;


import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.common.FileUtil;
import com.yl.knowledgelibray.common.MSWordPoiBuilder;
import com.yl.knowledgelibray.common.ResponseDataTool;
import com.yl.knowledgelibray.common.TreeBuilder;
import com.yl.knowledgelibray.domain.*;
import com.yl.knowledgelibray.service.*;
import org.apache.catalina.Session;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Created by luowanglin
 * Date: 2018/9/22
 * Time: 18:58
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping(value = "/")
public class KnowledgelibrayController {

    Logger logger = Logger.getLogger(this.getClass().getName());
    public static String uploadDirectory = System.getProperty("user.dir")+"/uploads/knowledge/";

    @Autowired
    KnowledgeTypeService knowledgeTypeService;
    @Autowired
    KnowledgeService knowledgeService;
    @Autowired
    UserService userService;
    @Autowired
    ProcessCategoryService processCategoryService;
    @Autowired
    ProcessCategoryContentService processCategoryContentService;
    @Autowired
    PSTCPlanProcessService pstcPlanProcessService;
    @Autowired
    KnowledgeFileService knowledgeFileService;
    @Autowired
    DeviceService deviceService;

    /**
    * 获取用户id
    * */
    @RequestMapping(value = "/userId",method = RequestMethod.POST)
    public HashMap getUserId(String username,String password) {
        if (username == null || password == null || "".equals(username) || "".equals(password)) {
            HashMap error = new HashMap();
            error.put("error","param is null");
            return ResponseDataTool.getTool().returnHashMapContent(error);
        }
        HashMap data = new HashMap();
        String userId = userService.findUserIdBy(username,password);
        if (userId == null) {
            User user = new User();
            user.setUserName(username);
            user.setPassword(password);
            userService.insertUser(user);
            userId = userService.findUserIdBy(username,password);
        }
        data.put("userId",userId);
        return ResponseDataTool.getTool().returnHashMapContent(data);
    }

    /**
    * 获取知识类型
    * */
    @RequestMapping(value = "/types",method = RequestMethod.GET)
    public HashMap getAllKnowledgeTypes(HttpServletRequest request,String userId) {
        // 获取全部目录节点
        String userid = (String) request.getSession().getAttribute("userId");
        List<KnowledgeType> nodes = knowledgeTypeService.findAllTypes(userid);
        // 拼装树形json字符串
        return ResponseDataTool.getTool().returnListContent(new TreeBuilder().buildTree(nodes));
    }

    /**
     * 添加知识树类型
     * Example:{"userId":"40288347661a413201661a4159050009","knowledgeType":"测试类型","modifi":true}
     * */
    @RequestMapping(value = "/addKnowledgeType",method = RequestMethod.POST)
    public HashMap addKnowledgeType(HttpServletRequest request,@RequestBody KnowledgeType knowledgeType) {
        String userid = (String) request.getSession().getAttribute("userId");
        if (knowledgeType == null || knowledgeType.getTypeCode() != null) {
            HashMap erro = new HashMap();
            erro.put("erro","body is null Or typecode not null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        logger.log(Level.INFO,JSON.toJSONString(knowledgeType));
        knowledgeType.setUserId(userid);
        knowledgeTypeService.addType(knowledgeType);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 删除知识类型
     * Example:["40288087662b1dc901662b1f060f0000"]
     * */
    @RequestMapping(value = "/deleteKnowledgeType",method = RequestMethod.DELETE)
    public HashMap deleteKnowledgeType(@RequestBody List<Integer> typeCodeS) {
        if (typeCodeS == null) {
            HashMap erro = new HashMap();
            erro.put("erro","typeCodes is null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        logger.log(Level.WARNING,JSON.toJSONString(typeCodeS));
        knowledgeTypeService.deleteBatchKnowledgeType(typeCodeS);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 修改知识类型
     * Example:{"userId":"40288347661a413201661a4159050009","knowledgeType":"测试型","modifi":true,"typeCode":"40288087662b1dc901662b2a8c900001"}
     * */
    @RequestMapping(value = "/updateKnowledgeType",method = RequestMethod.PUT)
    public HashMap updateKnowledgeType(HttpServletRequest request,@RequestBody KnowledgeType knowledgeType) {
        String userid = (String) request.getSession().getAttribute("userId");
        if (knowledgeType != null) {
            if (knowledgeType.getTypeCode() == null || knowledgeType.getTypeCode().equals("")) {
                HashMap erro = new HashMap();
                erro.put("error","typeCode is null");
                return ResponseDataTool.getTool().returnContent(erro);
            }
            logger.log(Level.INFO,JSON.toJSONString(knowledgeType));
            knowledgeType.setUserId(userid);
            return ResponseDataTool.getTool().returnContent(knowledgeTypeService.updateType(knowledgeType));
        }
        return ResponseDataTool.getTool().returnContent(false);
    }


    /**
    * 获取所有知识条目
    * */
    @RequestMapping(value = "/knowledge",method = RequestMethod.GET)
    public HashMap getAllKnowledge(HttpServletRequest request,String userId,String indexPage,String pageSize) {
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "50";
        }
        String userid = (String) request.getSession().getAttribute("userId");
        Pageable pageable = PageRequest.of(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize));
        return knowledgeService.findAllKnowledge(userid,pageable);
    }

    /**
    * 过滤知识条目
    * */
    @RequestMapping(value = "/knowledgeByType",method = RequestMethod.GET)
    public HashMap getKnowledgeByType(HttpServletRequest request,String userId,String type,String indexPage,String pageSize) {
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "50";
        }
        String userid = (String) request.getSession().getAttribute("userId");
        Pageable pageable = PageRequest.of(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize));
        if (type == null || type.isEmpty() || type.equals("")) {
            return knowledgeService.findAllKnowledge(userid,pageable);
        }
        return knowledgeService.findKnowledgeByType(type,userid,pageable);
    }

    /**
    * 添加知识条目
    * */
    @RequestMapping(value = "/addKnowledge",method = RequestMethod.POST)
    public HashMap addKnowledge(HttpServletRequest request,@RequestBody KnowledgeBody body) {
        logger.log(Level.WARNING,"添加："+JSON.toJSONString(body));
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("userId");
        String username = (String) session.getAttribute("userName");
        if (body.getKnowledge().getKnowType() == null || body.getKnowledge().getKnowType().equals("")) {
            HashMap erro = new HashMap();
            erro.put("erro","knowType is null");
            return ResponseDataTool.getTool().returnHashMapContent(erro);
        }
        //绑定设备
        if (body.getKnowledge().getDevices() != null && body.getKnowledge().getDevices().size() > 0) {
            for (Device device : body.getKnowledge().getDevices()) {
                device.setKnowledge(body.getKnowledge());
            }
        }
        body.getKnowledge().setUserId(userid);
        body.getKnowledge().setCreatName(username);
        Knowledge knowledge = knowledgeService.addKnowledge(body.getKnowledge());
        //保存计划列表
        if (body.getPlans() != null && body.getKnowledge().getKnowType().equals("PSTC") && body.getPlans().size() > 0) {
            for (PSTCPlanProcess ps:body.getPlans()) {
                ps.setKnowledgeId(knowledge.getKnowledgeId());
            }
            pstcPlanProcessService.addAPSTCPlanBatchProcess(body.getPlans());
        }
        //保存运维分类
        if (body.getProcesss() != null && body.getProcesss().size() > 0) {
            for (ProcessCategory pc: body.getProcesss()) {
                pc.setKnowledgeId(knowledge.getKnowledgeId());
                ProcessCategory pca = processCategoryService.addProcessCategory(pc);
                for (ProcessCategoryContent pcc : pc.getProcessCategoryContent()) {
                    pcc.setProcessId(pca.getProcessId());
                }
                List<ProcessCategoryContent> list = processCategoryContentService.addCategoryContent(pca.getProcessCategoryContent());
                for(ProcessCategoryContent pcc : list) {
                    KnowledgeFile kf = new KnowledgeFile();
                    kf.setContentId(pcc.getContentId());
                    kf.setFileName(pcc.getAttachLink());
                    knowledgeFileService.addFile(kf);
                }
            }
        }
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 获取知识条目
     * */
    @RequestMapping(value = "/getKnowledgeContent",method = RequestMethod.GET)
    public HashMap getKnowledgeContent(String knowledgeId) {
        Knowledge knowledge = knowledgeService.findKnowledgeById(knowledgeId);
        if (knowledge == null) {
            HashMap error = new HashMap();
            error.put("data",null);
            return ResponseDataTool.getTool().returnContent(error);
        }
        if (knowledge.getAttachId() != null && knowledge.getAttachId().isEmpty() == false) {
            String attachIdStr = knowledge.getAttachId();
            if (attachIdStr != null) {
                String[] attachIds = attachIdStr.split(",");
                List<Knowledge> plans = new LinkedList<>();
                for (String id : attachIds) {
                    Knowledge li = knowledgeService.findKnowledgeById(id);
                    if (li != null) {
                        li.setPlanContent(pstcPlanProcessService.findAllPlanProcessByKnowledgeId(id));
                    }
                    plans.add(li);
                }
                knowledge.setAttachKnowledge(plans);
            }
        }
        logger.log(Level.WARNING,JSON.toJSONString(knowledge));
        return ResponseDataTool.getTool().returnContent(knowledge);
    }

    /**
    * 删除知识条目
    * */
    @RequestMapping(value = "/deleteKnowledge",method = RequestMethod.DELETE)
    public HashMap deleteKnowledgeById(String knowledgeId) {
        List<ProcessCategory> pca = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        for (ProcessCategory pr: pca) {
            processCategoryContentService.deleteCategoryContentByProcessId(pr.getProcessId());
        }
        processCategoryService.deleteProcessCategoryBatch(knowledgeId);
        pstcPlanProcessService.deletePSTCPlanBatchProcess(knowledgeId);
        knowledgeService.deleteKnowledgeByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
    * 修改知识条目
    * */
    @RequestMapping(value = "/updataKnowledge",method = RequestMethod.POST)
    public HashMap updataKnowledge(HttpServletRequest request,@RequestBody KnowledgeBody body) {
        HttpSession session = request.getSession();
        String userid = (String) session.getAttribute("userId");
        String username = (String) session.getAttribute("userName");
        if (body.getKnowledge().getKnowledgeId() == null || body.getKnowledge().getKnowledgeId().equals("")) {
            HashMap erro = new HashMap();
            erro.put("error","knowledgeId is null");
            return ResponseDataTool.getTool().returnContent(erro);
        }
        logger.log(Level.INFO,"Body:"+JSON.toJSONString(body));
        body.getKnowledge().setUserId(userid);
        body.getKnowledge().setCreatName(username);
        //绑定设备
        if (body.getKnowledge().getDevices() != null && body.getKnowledge().getDevices().size() > 0) {
            for (Device device : body.getKnowledge().getDevices()) {
                device.setKnowledge(body.getKnowledge());
            }
        }
        deviceService.deleteAllDeviceByKnowledge(body.getKnowledge());
        knowledgeService.updataKnowledge(body.getKnowledge());
        pstcPlanProcessService.deletePSTCPlanBatchProcess(body.getKnowledge().getKnowledgeId());
        processCategoryService.deleteProcessCategoryBatch(body.getKnowledge().getKnowledgeId());
        if (body.getPlans() != null && body.getPlans().size() > -1){
            pstcPlanProcessService.updataPSTCPlanBatchProcess(body.getPlans());
        }
        if (body.getProcesss() != null && body.getProcesss().size() > -1){
            for (ProcessCategory pc: body.getProcesss()) {
                ProcessCategory pct = processCategoryService.updataProcessCategory(pc);
                for (ProcessCategoryContent pcc:pc.getProcessCategoryContent()) {
                        pcc.setProcessId(pct.getProcessId());
                        pcc.setContentId(pcc.getContentId());
                        ProcessCategoryContent processCategoryContent = processCategoryContentService.updataCategoryContent(pcc);
//                        if (processCategoryContent.getAttachLink() == null || processCategoryContent.getAttachLink().equals("")){
//                            logger.log(Level.INFO,"删除文件...");
//                            knowledgeFileService.deletFile(pcc.getContentId());
//                        }else{
//                            KnowledgeFile knowledgeFile = knowledgeFileService.findKnowledgeFileBy(pcc.getContentId());
//                            logger.log(Level.INFO,"attach_link:"+processCategoryContent.getAttachLink());
//                            if (knowledgeFile != null) {
//                                knowledgeFileService.updateFile(knowledgeFile);
//                            }else{
//                                KnowledgeFile kf = new KnowledgeFile();
//                                kf.setFileName(processCategoryContent.getAttachLink());
//                                kf.setContentId(processCategoryContent.getContentId());
//                                knowledgeFileService.addFile(kf);
//                            }
//                        }
                }
            }
        }
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 查询用户下的所有计划
     * */
    @RequestMapping(value = "/findAllPlans",method = RequestMethod.GET)
    public HashMap findAllPlans(HttpServletRequest request,String userId) {
        String userid = (String) request.getSession().getAttribute("userId");
        return ResponseDataTool.getTool().returnContent(knowledgeService.findKnowledgeByType("PSTC",userid));
    }

    /**
     * 获取计划内容
     * */
    @RequestMapping(value = "/findPlanContent",method = RequestMethod.GET)
    public HashMap findPlanContent(String knowledgeId) {
        List<PSTCPlanProcess> list = pstcPlanProcessService.findAllPlanProcessByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnContent(list);
    }

    /**
     * 更新计划内容
     * */
    @RequestMapping(value = "/updataPlanContent",method = RequestMethod.PUT)
    public HashMap updatePlanContent(@RequestBody List<PSTCPlanProcess> list){
        logger.log(Level.WARNING,JSON.toJSONString(list));
        return ResponseDataTool.getTool().returnContent(pstcPlanProcessService.updataPSTCPlanBatchProcess(list));
    }

    /**
     * 新增运维分类
     * @param description 可传空
     * */
    @RequestMapping(value = "/addCategory",method = RequestMethod.PUT)
    public HashMap addOpsCategory(String description,String proType,String knowledgeId) {
        ProcessCategory processCategory = new ProcessCategory();
        processCategory.setDescription(description);
        processCategory.setProType(proType);
        processCategory.setKnowledgeId(knowledgeId);
        ProcessCategory entity = processCategoryService.addProcessCategory(processCategory);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 更新运维分类
     * */
    @RequestMapping(value = "/updateCategory",method = RequestMethod.PUT)
    public HashMap updataOpsCategory(String proccessId,String knowledgeId,String proType,String description) {
        if (proccessId == null || proccessId == "") {
            HashMap data = new HashMap();
            data.put("message","processId is null");
            return ResponseDataTool.getTool().returnHashMapContent(data);
        }
        ProcessCategory processCategory = new ProcessCategory();
        processCategory.setDescription(description);
        processCategory.setProType(proType);
        processCategory.setKnowledgeId(knowledgeId);
        processCategory.setProcessId(proccessId);
        processCategoryService.updataProcessCategory(processCategory);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 查找所有运维分类
     * */
    @RequestMapping(value = "/findAllCategory",method = RequestMethod.GET)
    public HashMap findAllOpsCategory(String knowledgeId) {
        List<ProcessCategory> data = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        return ResponseDataTool.getTool().returnListContent(data);
    }

    /**
     * 删除运维分类
     * */
    @RequestMapping(value = "/deleteCategory",method = RequestMethod.DELETE)
    public HashMap deleteOpsCategory(String process_id) {
        processCategoryService.deleteProcessCategoryByProcessId(process_id);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 添加运维内容
     * */
    @RequestMapping(value = "/addCategoryContent",method = RequestMethod.POST)
    public HashMap addCategoryContent(Integer num,String processId,String processType,String title,String attachLink) {
        ProcessCategoryContent processCategoryContent = new ProcessCategoryContent();
        processCategoryContent.setNum(num);
        processCategoryContent.setProcessId(processId);
        processCategoryContent.setProcessType(processType);
        processCategoryContent.setTitle(title);
        processCategoryContent.setAttachLink(attachLink);
        ProcessCategoryContent entity = processCategoryContentService.addCategoryContent(processCategoryContent);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 批量添加运维内容
     * */
    @RequestMapping(value = "/addBatchCategoryContent",method = RequestMethod.POST)
    public HashMap addBatchCategoryContent(List<ProcessCategoryContent> contents) {
        List<ProcessCategoryContent> lists = processCategoryContentService.addCategoryContent(contents);
        return ResponseDataTool.getTool().returnContent(lists);
    }

    /**
     * 查找运维内容
     * */
    @RequestMapping(value = "/findCategoryContetn",method = RequestMethod.GET)
    public HashMap findCategoryContetn(String processId) {
        return ResponseDataTool.getTool().returnListContent(processCategoryContentService.findCategoryContentByProcessId(processId));
    }

    /**
     * 删除运维内容
     * */
    @RequestMapping(value = "/deleteCategoryContent",method = RequestMethod.DELETE)
    public HashMap deleteCategoryContent(String contentId) {
        processCategoryContentService.deleteCategoryContentByContentId(contentId);
        return ResponseDataTool.getTool().returnContent(true);
    }

    /**
     * 更新运维内容
     * */
    @RequestMapping(value = "/updataCategoryContent",method = RequestMethod.PUT)
    public HashMap updataCategoryContent(String contentId,Integer num,String processId,String processType,String title,String attachLink) {
        if (contentId == null || contentId.equals("")) {
            HashMap data = new HashMap();
            data.put("error","contentId is null");
            return ResponseDataTool.getTool().returnHashMapContent(data);
        }
        ProcessCategoryContent pcc = new ProcessCategoryContent();
        pcc.setContentId(contentId);
        pcc.setNum(num);
        pcc.setProcessId(processId);
        pcc.setProcessType(processType);
        pcc.setTitle(title);
        pcc.setAttachLink(attachLink);
        ProcessCategoryContent entity = processCategoryContentService.updataCategoryContent(pcc);
        return ResponseDataTool.getTool().returnContent(entity);
    }

    /**
     * 文件上传
     * */
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    public HashMap uploadFile(@RequestParam("file") MultipartFile file,
                           HttpServletRequest request) {
        String contentType = file.getContentType();
        String fileName = file.getOriginalFilename();
        try {
            FileUtil.uploadFile(file.getBytes(), uploadDirectory, fileName);
        } catch (Exception e) {
            // TODO: handle exception
        }
        //返回json
        HashMap data = new HashMap();
        String fileUrl = request.getScheme() + "://"+request.getServerName()+":" +
                request.getServerPort() +request.getContextPath()+"/file/"+ fileName;
        data.put("data",fileUrl);
        return ResponseDataTool.getTool().returnHashMapContent(data);
    }

    /**
     * 删除文件
     * */
    @RequestMapping(value = "/deleteFile",method = RequestMethod.DELETE)
    public HashMap deleteFile(HttpServletRequest request,String contentId) {
        if (request == null) {
            return ResponseDataTool.getTool().returnContent(false);
        }
        String filePath = request.getSession().getServletContext().getRealPath("fileupload/");
        logger.log(Level.WARNING,"deleteFilePath:"+filePath);
        File folder = new File(filePath);
        Boolean isDelete = false;
        if (folder != null) {
            File[] files = folder.listFiles();
            if (files != null) {
                for(File file:files){
                    logger.log(Level.WARNING,file.getName()+"..."+contentId);
                    if(file.getName().split("\\.")[0].equals(contentId)){
                        isDelete = file.delete();
                    }
                }
            }
        }
        return ResponseDataTool.getTool().returnContent(isDelete);
    }

    /**
     * Word文件生成下载
     * */
    @RequestMapping(value = "/genWord",method = RequestMethod.GET)
    public void getWordFile(HttpServletResponse response, HttpServletRequest request,String knowledgeId) throws IOException {
         Knowledge knowledge = knowledgeService.findKnowledgeById(knowledgeId);
        if (knowledge == null) {
            HashMap error = new HashMap();
            error.put("data",null);
            return ;
        }
        if (knowledge.getAttachId() != null && knowledge.getAttachId().isEmpty() == false) {
            String attachIdStr = knowledge.getAttachId();
            if (attachIdStr != null) {
                String[] attachIds = attachIdStr.split(",");
                List<Knowledge> plans = new LinkedList<>();
                for (String id : attachIds) {
                    Knowledge li = knowledgeService.findKnowledgeById(id);
                    if (li != null) {
                        li.setPlanContent(pstcPlanProcessService.findAllPlanProcessByKnowledgeId(id));
                    }
                    plans.add(li);
                }
                knowledge.setAttachKnowledge(plans);
            }
        }
         String filePath = request.getServletContext().getRealPath("/") + "template";
         File fil= File.createTempFile("temp",".docx");
         logger.log(Level.INFO,fil.getAbsolutePath() + "~~~" + fil.getCanonicalPath() + "~~~" + fil.getPath());
        if(!fil.exists()){
            logger.log(Level.WARNING,"文件不存在。。。");
         try {
                fil.createNewFile();
            } catch (IOException e){
              e.printStackTrace();
            }
        }
        MSWordPoiBuilder builder = new MSWordPoiBuilder();
        XWPFDocument doc = builder.creatmswordfile(knowledge);
        //写出文档
        OutputStream os = null;
        try {
            os = new FileOutputStream(fil);
        } catch (FileNotFoundException e) {
            logger.log(Level.WARNING,"wejian 没有发现...");
            e.printStackTrace();
        }
        //把doc输出到输出流
        try {
            doc.write(os);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //获得请求文件名
         System.out.println(filePath);
         //以下载方式打开
         response.setHeader("Content-Disposition", "attachment;filename="+knowledge.getKnowledgeId()+".docx");
         FileInputStream fis =  new FileInputStream(fil);
         //写出
         ServletOutputStream out = response.getOutputStream();
         //定义读取缓冲区
         byte buffer[] = new byte[1024];
         //定义读取长度
         int len = 1024;
         //循环读取
         while((len = fis.read(buffer))!=-1){
             out.write(buffer,0,len);
         }
         //释放资源
         fis.close();
         os.flush();
         os.close();
         out.flush();
         out.close();
         fil.delete();
         builder.close(os);
    }

    /**
     * 模糊查询
     * */
    @RequestMapping(value = "/findKnowledgeByKeyword",method = RequestMethod.GET)
    public HashMap findKnowledgeByFilter(HttpServletRequest request,String keyword,String knowType,String indexPage,String pageSize) {
        String userid = (String) request.getSession().getAttribute("userId");
        if (indexPage == null || pageSize == null || indexPage.isEmpty() || pageSize.isEmpty()) {
            indexPage = "1";
            pageSize = "50";
        }
        Pageable pageable = PageRequest.of(Integer.parseInt(indexPage)-1,Integer.parseInt(pageSize));
        return knowledgeService.findKnowledgeByKey(keyword,knowType,userid,pageable);
    }


}
