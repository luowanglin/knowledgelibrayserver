package com.yl.knowledgelibray.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/28
 * Time: 08:40
 * To change this template use File | Settings | File Templates.
 */
public class KnowledgeBody implements Serializable {

    private Knowledge knowledge;

    private List<PSTCPlanProcess> plans;

    private List<ProcessCategory> processs;

    public KnowledgeBody() {}

    public Knowledge getKnowledge() {
        return knowledge;
    }

    public void setKnowledge(Knowledge knowledge) {
        this.knowledge = knowledge;
    }

    public List<PSTCPlanProcess> getPlans() {
        return plans;
    }

    public void setPlans(List<PSTCPlanProcess> plans) {
        this.plans = plans;
    }

    public List<ProcessCategory> getProcesss() {
        return processs;
    }

    public void setProcesss(List<ProcessCategory> processs) {
        this.processs = processs;
    }
}
