package com.yl.knowledgelibray.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_file")
public class KnowledgeFile implements Serializable {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    private Integer id;

    private String contentId;

    private String fileName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        if (fileName != null) {
            String[] fns = fileName.split("\\/");
            fileName = fns[fns.length-1];
        }
        this.fileName = fileName;
    }

}
