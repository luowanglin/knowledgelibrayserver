package com.yl.knowledgelibray.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 16:51
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_process_category")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class ProcessCategory implements Serializable {

    @GeneratedValue(generator = "jpa-uuid")
    @Id
    private String processId;

    private String knowledgeId;

    @Column(name = "pro_type")
    private String proType;

    private String description;

    @Transient
    private List<ProcessCategoryContent> processCategoryContent;

    public ProcessCategory() {}

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getKnowledgeId() {
        return knowledgeId;
    }

    public void setKnowledgeId(String knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getProType() {
        return proType;
    }

    public void setProType(String proType) {
        this.proType = proType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ProcessCategoryContent> getProcessCategoryContent() {
        return processCategoryContent;
    }

    public void setProcessCategoryContent(List<ProcessCategoryContent> processCategoryContent) {
        this.processCategoryContent = processCategoryContent;
    }
}
