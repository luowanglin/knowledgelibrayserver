package com.yl.knowledgelibray.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_process_category_content")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class ProcessCategoryContent {

    @GeneratedValue(generator = "jpa-uuid")
    @Id
    private String contentId;

    private Integer num;

    private String processId;

    private String processType;

    private String title;

    private String attachLink;

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAttachLink() {
        return attachLink;
    }

    public void setAttachLink(String attachLink) {
        this.attachLink = attachLink;
    }
}
