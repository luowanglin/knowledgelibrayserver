package com.yl.knowledgelibray.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 08:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "tb_user")
@GenericGenerator(name = "jpa-uuid", strategy = "uuid")
public class User implements Serializable {

    @Id
    @GeneratedValue(generator = "jpa-uuid")
    private String userId;

    private String userName;

    private String password;

    public String getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
