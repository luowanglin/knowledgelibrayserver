package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.Device;
import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.domain.KnowledgeFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */
public interface DeviceRepository extends JpaRepository<Device,Integer> {

    @Modifying
    @Query(value="delete from tb_device where knowledge_id =:knowledgeId",nativeQuery = true)
    public void deleteAllByKnowledge(@Param("knowledgeId") String knowledgeId);

}
