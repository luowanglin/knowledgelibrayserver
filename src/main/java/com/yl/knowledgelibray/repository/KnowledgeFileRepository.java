package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.KnowledgeFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 22:01
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeFileRepository extends JpaRepository<KnowledgeFile,Integer> {

    @Modifying
    @Query(value = "select * from tb_file where content_id is null or trim(content_id)=''",nativeQuery = true)
    public List<KnowledgeFile> findFile();

    public void deleteKnowledgeFileByContentId(String contentId);

    public KnowledgeFile findKnowledgeFileByContentId(String contentId);

}
