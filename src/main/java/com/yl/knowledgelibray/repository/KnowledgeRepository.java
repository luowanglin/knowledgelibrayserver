package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.Knowledge;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:45
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeRepository extends JpaRepository<Knowledge,Integer> {

    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.user_id = :userId order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge where tb_knowledge.user_id = :userId",
            nativeQuery = true)
    public Page<Knowledge> findAllKnowledgeByUserId(@Param("userId") String userId,Pageable pageable);

    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.know_type = :knowledgeType and tb_knowledge.user_id = :userId order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge where know_type = :knowledgeType and user_id = :userId",
            nativeQuery = true)
    public Page<Knowledge> findKnowledgeByType(@Param("knowledgeType")String knowledgeType,@Param("userId")String userId,Pageable pageable);

    @Modifying
    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.know_type = :knowledgeType and tb_knowledge.user_id = :userId order by tb_knowledge.create_time DESC",nativeQuery = true)
    public List<Knowledge> findKnowledgeByType(@Param("knowledgeType")String knowledgeType,@Param("userId")String userId);

    public void deleteKnowledgeByKnowledgeId(String knowledgeId);

    public Knowledge findKnowledgeByKnowledgeId(String knowledgeId);

    @Query(value = "select tb_knowledge.* from tb_knowledge where tb_knowledge.user_id = :userId and ((tb_knowledge.know_type like %:key%) or (tb_knowledge.time like %:key%) or (tb_knowledge.creat_name like %:key%) or (tb_knowledge.title like %:key%) or (tb_knowledge.serial_number like %:key%)) order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge where tb_knowledge.know_type like %:key% and tb_knowledge.user_id = :userId",
            nativeQuery = true)
    public Page<Knowledge> findKnowledgeByKeyWord(@Param("key")String key,@Param("userId")String userId,Pageable pageable);

    @Query(value = "select tb_knowledge.* from tb_knowledge where (tb_knowledge.know_type like %:key% or tb_knowledge.time like %:key% or tb_knowledge.creat_name like %:key% or tb_knowledge.title like %:key% or tb_knowledge.serial_number like %:key%) and tb_knowledge.know_type = :knowtype and tb_knowledge.user_id = :userId order by tb_knowledge.create_time DESC \n-- #pageable\n",
            countQuery = "SELECT count(*) FROM tb_knowledge where tb_knowledge.know_type like %:key% and tb_knowledge.user_id = :userId",
            nativeQuery = true)
    public Page<Knowledge> findKnowledgeByKeyWord(@Param("key")String key,@Param("knowtype")String knowtype,@Param("userId")String userId,Pageable pageable);

    @Modifying
    @Query(value = "select tb_knowledge.* from tb_knowledge where (tb_knowledge.know_type like %:key% or tb_knowledge.time like %:key% or tb_knowledge.creat_name like %:key% or tb_knowledge.title like %:key% or tb_knowledge.serial_number like %:key%) and tb_knowledge.user_id =:userId order by tb_knowledge.create_time DESC ",nativeQuery = true)
    public List<Knowledge> findKnowledgeByKeyWord(@Param("key")String key,@Param("userId")String userId);

}
