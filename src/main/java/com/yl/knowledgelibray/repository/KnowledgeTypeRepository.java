package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.KnowledgeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:59
 * To change this template use File | Settings | File Templates.
 */
//@RepositoryDefinition(domainClass = KnowledgeType.class,idClass = Integer.class)
@RepositoryRestResource(collectionResourceRel = "type",path = "type")
public interface KnowledgeTypeRepository extends JpaRepository<KnowledgeType,Integer> {

    @Modifying
    @Query(value = "insert into tb_type(father_code,knowledge_type,modifi) values(?1,?2,?3,?4)",nativeQuery = true)
    public void addType(Integer fatherCode,String knowledgeType,Boolean modifi);


    @Modifying
    @Query(value="select * from tb_type where user_id = :userId or user_id is null order by type_code ",nativeQuery = true)
    public List<KnowledgeType> findAllTypeByUserId(@Param("userId")String userId);

    public List<KnowledgeType> findAllByFatherCode(Integer fatherCode);

    public void deleteByTypeCode(Integer typeCode);

//    @Modifying
//    @Query(value="delete from tb_type e where e.type_code in (?1) ")
//    public void deleteByTypeCodes(List<Integer> typeCodes);

}
