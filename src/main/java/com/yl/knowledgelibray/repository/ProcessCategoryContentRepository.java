package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.RepositoryDefinition;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 17:22
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryContentRepository extends JpaRepository<ProcessCategoryContent,Integer> {
    public List<ProcessCategoryContent> findProcessCategoryContentsByProcessIdOrderByNum(String processId);
    public void deleteByContentId(String contentId);
    public void deleteAllByProcessId(String processId);
}
