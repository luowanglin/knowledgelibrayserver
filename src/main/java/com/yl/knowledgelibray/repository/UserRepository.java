package com.yl.knowledgelibray.repository;

import com.yl.knowledgelibray.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.HashMap;
import java.util.UUID;


/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 16:02
 * To change this template use File | Settings | File Templates.
 */
public interface UserRepository extends JpaRepository<User,UUID> {

    @Query(value = "select user_id from tb_user where user_name = :userName and password = :password",nativeQuery = true)
    public String findIdByInfor(@Param("userName") String userName, @Param("password") String password);

    @Modifying
    @Query(value = "update tb_user set password = :password where user_id = :userId",nativeQuery = true)
    public void configPasswordById(@Param("password")String password,@Param("userId")String userId);

}
