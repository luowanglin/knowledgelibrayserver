package com.yl.knowledgelibray.service;


import com.yl.knowledgelibray.domain.Knowledge;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
public interface DeviceService {

    public void deleteAllDeviceByKnowledge(Knowledge knowledge);

}
