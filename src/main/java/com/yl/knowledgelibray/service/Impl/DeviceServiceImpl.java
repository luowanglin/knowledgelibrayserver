package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.repository.DeviceRepository;
import com.yl.knowledgelibray.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by luowanglin
 * Date: 2018/10/10
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DeviceServiceImpl implements DeviceService {

    @Autowired
    DeviceRepository deviceRepository;

    @Transactional
    @Override
    public void deleteAllDeviceByKnowledge(Knowledge knowledge) {
        deviceRepository.deleteAllByKnowledge(knowledge.getKnowledgeId());
    }

}
