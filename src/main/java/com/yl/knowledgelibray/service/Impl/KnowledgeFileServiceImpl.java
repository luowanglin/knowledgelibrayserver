package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.KnowledgeFile;
import com.yl.knowledgelibray.repository.KnowledgeFileRepository;
import com.yl.knowledgelibray.service.KnowledgeFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.logging.Level;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 22:04
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KnowledgeFileServiceImpl implements KnowledgeFileService {


    @Autowired
    KnowledgeFileRepository knowledgeFileRepository;

    @Override
    public KnowledgeFile addFile(KnowledgeFile file) {
        clean();
        return knowledgeFileRepository.save(file);
    }

    @Override
    public void deletFile(String contentId) {
        String filePath = System.getProperty("user.dir")+"/uploads/knowledge/";
        KnowledgeFile knowledgeFile = knowledgeFileRepository.findKnowledgeFileByContentId(contentId);
        File folder = new File(filePath);
        if (folder != null) {
            File[] files = folder.listFiles();
            if (files != null) {
                for(File f : files){
                    if (f != null && knowledgeFile != null){
                        if(f.getName().equals(knowledgeFile.getFileName())){
                            f.delete();
                        }
                    }
                }
            }
        }
        knowledgeFileRepository.deleteKnowledgeFileByContentId(contentId);
    }

    @Override
    public KnowledgeFile updateFile(KnowledgeFile file) {
        clean();
        return knowledgeFileRepository.saveAndFlush(file);
    }

    @Override
    public KnowledgeFile findKnowledgeFileBy(String contentId) {
        return knowledgeFileRepository.findKnowledgeFileByContentId(contentId);
    }

    /**
     * 清理野文件
     * */
    private void clean() {
        List<KnowledgeFile> list = knowledgeFileRepository.findFile();
        String filePath = System.getProperty("user.dir")+"/uploads/knowledge/";
        for (KnowledgeFile kf : list) {
            File folder = new File(filePath);
            if (folder != null) {
                File[] files = folder.listFiles();
                if (files != null) {
                    for(File f : files){
                        if(f.getName().equals(kf.getFileName())){
                            f.delete();
                        }
                    }
                }
            }
        }
        knowledgeFileRepository.deleteInBatch(list);
    }
}
