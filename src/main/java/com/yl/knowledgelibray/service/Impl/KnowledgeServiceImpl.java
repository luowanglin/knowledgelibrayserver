package com.yl.knowledgelibray.service.Impl;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.common.ResponseDataTool;
import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import com.yl.knowledgelibray.domain.ProcessCategory;
import com.yl.knowledgelibray.repository.KnowledgeRepository;
import com.yl.knowledgelibray.repository.PSTCPlanProcessRepository;
import com.yl.knowledgelibray.service.KnowledgeService;
import com.yl.knowledgelibray.service.ProcessCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KnowledgeServiceImpl implements KnowledgeService {

    @Autowired
    KnowledgeRepository knowledgeRepository;
    @Autowired
    PSTCPlanProcessServiceImpl pstcPlanProcessService;
    @Autowired
    ProcessCategoryService processCategoryService;

    @Override
    public HashMap findAllKnowledge(String userId, Pageable pageable) {
        Page<Knowledge> pages = knowledgeRepository.findAllKnowledgeByUserId(userId,pageable);
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }

    @Override
    public HashMap findKnowledgeByType(String type, String userId, Pageable pageable) {
        Page<Knowledge> pages = knowledgeRepository.findKnowledgeByType(type,userId,pageable);
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }

    @Override
    public List<Knowledge> findKnowledgeByType(String type, String userId) {
        List<Knowledge> list = knowledgeRepository.findKnowledgeByType(type,userId);
        return list;
    }

    @Override
    public Knowledge findKnowledgeById(String knowledgeId) {
        Knowledge knowledge = knowledgeRepository.findKnowledgeByKnowledgeId(knowledgeId);
        List<PSTCPlanProcess> planContents = pstcPlanProcessService.findAllPlanProcessByKnowledgeId(knowledgeId);
        List<ProcessCategory> processContents = processCategoryService.findProcessCategoryByKnowledgeId(knowledgeId);
        if (knowledge != null){
            knowledge.setPlanContent(planContents);
            knowledge.setProcessContent(processContents);
        }
        return knowledge;
    }

    @Override
    public HashMap findKnowledgeByKey(String keyword,String knowType,String userId,Pageable pageable) {
        Page<Knowledge> pages = null;
        if (knowType == null || knowType.isEmpty()) {
            pages = knowledgeRepository.findKnowledgeByKeyWord(keyword,userId,pageable);
        }else {
            pages = knowledgeRepository.findKnowledgeByKeyWord(keyword,knowType,userId,pageable);
        }
        return ResponseDataTool.getTool().returnPageableData(pageable,pages);
    }
    @Override
    @Transactional
    public Knowledge addKnowledge(Knowledge knowledge) {
        return knowledgeRepository.save(knowledge);
    }

    @Override
    @Transactional
    public void deleteKnowledgeByKnowledgeId(String knowledgeId) {
        knowledgeRepository.deleteKnowledgeByKnowledgeId(knowledgeId);
    }

    @Override
    @Transactional
    public Knowledge updataKnowledge(Knowledge knowledge) {
        return knowledgeRepository.saveAndFlush(knowledge);
    }

}
