package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.KnowledgeType;
import com.yl.knowledgelibray.repository.KnowledgeTypeRepository;
import com.yl.knowledgelibray.service.KnowledgeTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/22
 * Time: 19:57
 * To change this template use File | Settings | File Templates.
 */
@Service
public class KnowledgeTypeServiceImpl implements KnowledgeTypeService {

    @Autowired
    KnowledgeTypeRepository knowledgeTypeRepository;

    @Override
    @Transactional
    public KnowledgeType addType(KnowledgeType knowledgeType) {
        return knowledgeTypeRepository.save(knowledgeType);
    }

    @Override
    @Transactional
    public KnowledgeType updateType(KnowledgeType knowledgeType) {
        return knowledgeTypeRepository.saveAndFlush(knowledgeType);
    }

    @Override
    public List<KnowledgeType> findAllTypes(String userid) {
        return knowledgeTypeRepository.findAllTypeByUserId(userid);
    }

    @Override
    @Transactional
    public void deleteKnowledgeType(Integer typeCode) {
        List<KnowledgeType> lis = knowledgeTypeRepository.findAllByFatherCode(typeCode);
        for (KnowledgeType kt: lis) {
            knowledgeTypeRepository.deleteByTypeCode(kt.getTypeCode());
        }
        knowledgeTypeRepository.deleteByTypeCode(typeCode);
    }

    @Override
    @Transactional
    public void deleteBatchKnowledgeType(List<Integer> typeCodes) {
        for (Integer code : typeCodes) {
            knowledgeTypeRepository.deleteByTypeCode(code);
        }
    }


}
