package com.yl.knowledgelibray.service.Impl;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import com.yl.knowledgelibray.repository.PSTCPlanProcessRepository;
import com.yl.knowledgelibray.service.PSTCPlanProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/23
 * Time: 23:20
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PSTCPlanProcessServiceImpl implements PSTCPlanProcessService {

    @Autowired
    PSTCPlanProcessRepository pstcPlanProcessRepository;

    @Override
    @Transactional
    public List<PSTCPlanProcess> addAPSTCPlanBatchProcess(List<PSTCPlanProcess> processes) {
        return pstcPlanProcessRepository.saveAll(processes);
    }

    @Override
    @Transactional
    public PSTCPlanProcess addPSTCPlanProcess(PSTCPlanProcess process) {
        return pstcPlanProcessRepository.save(process);
    }

    @Override
    @Transactional
    public void updataPSTCPlanProcess(PSTCPlanProcess pstcPlanProcess) {
        pstcPlanProcessRepository.saveAndFlush(pstcPlanProcess);
    }

    @Override
    @Transactional
    public List<PSTCPlanProcess> updataPSTCPlanBatchProcess(List<PSTCPlanProcess> processes) {
        return pstcPlanProcessRepository.saveAll(processes);
    }

    @Override
    @Transactional
    public void deletePSTCPlanProcessById(Integer id) {
        pstcPlanProcessRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deletePSTCPlanBatchProcess(String knowledgeId) {
        pstcPlanProcessRepository.deleteAllByKnowledgeId(knowledgeId);
    }

    @Override
    public List<PSTCPlanProcess> findAllPlanProcessByKnowledgeId(String knowledgeId) {
        return pstcPlanProcessRepository.findAllByKnowledgeIdOrderBySeqNum(knowledgeId);
    }
}
