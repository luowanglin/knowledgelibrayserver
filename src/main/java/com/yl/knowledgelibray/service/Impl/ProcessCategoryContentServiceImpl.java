package com.yl.knowledgelibray.service.Impl;

import com.alibaba.fastjson.JSON;
import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import com.yl.knowledgelibray.repository.ProcessCategoryContentRepository;
import com.yl.knowledgelibray.service.ProcessCategoryContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/26
 * Time: 01:50
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProcessCategoryContentServiceImpl implements ProcessCategoryContentService {

    @Autowired
    ProcessCategoryContentRepository pccr;

    @Override
    public List<ProcessCategoryContent> findCategoryContentByProcessId(String processId) {
        return pccr.findProcessCategoryContentsByProcessIdOrderByNum(processId);
    }

    @Override
    @Transactional
    public void deleteCategoryContentByContentId(String contentId) {
        pccr.deleteByContentId(contentId);
    }

    @Override
    @Transactional
    public void deleteCategoryContentByProcessId(String processId) {
        pccr.deleteAllByProcessId(processId);
    }

    @Override
    @Transactional
    public ProcessCategoryContent updataCategoryContent(ProcessCategoryContent processCategoryContent) {
        return pccr.saveAndFlush(processCategoryContent);
    }

    @Override
    @Transactional
    public List<ProcessCategoryContent> addCategoryContent(List<ProcessCategoryContent> contents) {
        return pccr.saveAll(contents);
    }

    @Override
    @Transactional
    public ProcessCategoryContent addCategoryContent(ProcessCategoryContent content) {
        return pccr.save(content);
    }
}
