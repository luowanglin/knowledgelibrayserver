package com.yl.knowledgelibray.service.Impl;

import com.yl.knowledgelibray.domain.User;
import com.yl.knowledgelibray.repository.UserRepository;
import com.yl.knowledgelibray.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 16:24
 * To change this template use File | Settings | File Templates.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public void insertUser(User user) {
        userRepository.save(user);
    }

    @Override
    @Transactional
    public String findUserIdBy(String userName, String password) {
        return userRepository.findIdByInfor(userName,password);
    }

    @Override
    @Transactional
    public void configPasswordById(String password,String userId) {
        userRepository.configPasswordById(password,userId);
    }
}
