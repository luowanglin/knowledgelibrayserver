package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.KnowledgeFile;

/**
 * Created by luowanglin
 * Date: 2018/10/4
 * Time: 22:03
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeFileService {

    public KnowledgeFile addFile(KnowledgeFile file);
    public void deletFile(String contentId);
    public KnowledgeFile updateFile(KnowledgeFile file);
    public KnowledgeFile findKnowledgeFileBy(String contentId);
}
