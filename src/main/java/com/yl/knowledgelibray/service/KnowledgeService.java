package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.Knowledge;
import org.springframework.data.domain.Pageable;

import java.util.HashMap;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 20:47
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeService {
    public HashMap findAllKnowledge(String userId, Pageable pageable);
    public HashMap findKnowledgeByType(String type,String userId, Pageable pageable);
    public List<Knowledge> findKnowledgeByType(String type,String userId);
    public Knowledge findKnowledgeById(String knowledgeId);
    public HashMap findKnowledgeByKey(String keyword,String knowType,String userId,Pageable pageable);
    public Knowledge addKnowledge(Knowledge knowledge);
    public void deleteKnowledgeByKnowledgeId(String knowledgeId);
    public Knowledge updataKnowledge(Knowledge knowledge);
}
