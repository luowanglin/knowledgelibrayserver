package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.Knowledge;
import com.yl.knowledgelibray.domain.KnowledgeType;
import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 21:00
 * To change this template use File | Settings | File Templates.
 */
public interface KnowledgeTypeService {

    public KnowledgeType addType(KnowledgeType knowledgeType);

    public KnowledgeType updateType(KnowledgeType knowledgeType);

    public List<KnowledgeType> findAllTypes(String userid);

    public void deleteKnowledgeType(Integer typeCode);

    public void deleteBatchKnowledgeType(List<Integer> typeCodes);
}
