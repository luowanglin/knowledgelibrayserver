package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.PSTCPlanProcess;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:52
 * To change this template use File | Settings | File Templates.
 */
public interface PSTCPlanProcessService {
    public List<PSTCPlanProcess> addAPSTCPlanBatchProcess(List<PSTCPlanProcess> processes);
    public PSTCPlanProcess addPSTCPlanProcess(PSTCPlanProcess process);
    public void updataPSTCPlanProcess(PSTCPlanProcess pstcPlanProcess);
    public List<PSTCPlanProcess> updataPSTCPlanBatchProcess(List<PSTCPlanProcess> processes);
    public void deletePSTCPlanProcessById(Integer id);
    public void deletePSTCPlanBatchProcess(String knowledgeId);
    public List<PSTCPlanProcess> findAllPlanProcessByKnowledgeId(String knowledgeId);
}
