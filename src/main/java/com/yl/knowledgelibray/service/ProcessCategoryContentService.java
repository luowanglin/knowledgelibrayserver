package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.ProcessCategory;
import com.yl.knowledgelibray.domain.ProcessCategoryContent;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryContentService {

    public List<ProcessCategoryContent> findCategoryContentByProcessId(String processId);
    public void deleteCategoryContentByContentId(String contentId);
    public void deleteCategoryContentByProcessId(String processId);
    public ProcessCategoryContent updataCategoryContent(ProcessCategoryContent processCategoryContent);
    public List<ProcessCategoryContent> addCategoryContent(List<ProcessCategoryContent> contents);
    public ProcessCategoryContent addCategoryContent(ProcessCategoryContent content);

}
