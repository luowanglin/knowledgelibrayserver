package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.ProcessCategory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by luowanglin
 * Date: 2018/9/21
 * Time: 20:51
 * To change this template use File | Settings | File Templates.
 */
public interface ProcessCategoryService {

    public ProcessCategory addProcessCategory(ProcessCategory processCategory);
    public void deleteProcessCategoryByProcessId(String processId);
    public void deleteProcessCategoryBatch(String knowledgeId);
    public List<ProcessCategory> findProcessCategoryByKnowledgeId(String knowledgeId);
    public ProcessCategory updataProcessCategory(ProcessCategory processCategory);

}
