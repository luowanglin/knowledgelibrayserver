package com.yl.knowledgelibray.service;

import com.yl.knowledgelibray.domain.User;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by luowanglin
 * Date: 2018/9/25
 * Time: 16:20
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    public void insertUser(User user);
    public String findUserIdBy(String userName, String password);
    public void configPasswordById(String password, String userId);
}
